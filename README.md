Oxygen Refit 2 icon theme
=========================

OxygenRefit2 is an icon-compilation primarly made for my desktop. I compiled it based upon Oxygen and others.

Last Version in 2010, what now?
===============================

Yes, I decided to restart compilation of the set now in February 2015. Don't expect anything soon, but a version 2.6.0 is definitively on it's way. Adding new stuff for GTK+3, KDE4 and more.
